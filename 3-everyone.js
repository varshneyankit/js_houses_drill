function everyone(data) {
  try {
    if (typeof data != "object") {
      throw new Error("Data file is incorrect");
    } else {
      const housesData = data.houses;

      const peopleNames = housesData.reduce((accumulator, houseData) => {
        const familyMembersData = houseData.people;
        const familyMemberNames = familyMembersData.map(
          (memberData) => memberData.name
        );

        accumulator = accumulator.concat(familyMemberNames);

        return accumulator;
      }, []);

      return peopleNames;
    }
  } catch (error) {
    console.log(error);
  }
}

module.exports = everyone;
