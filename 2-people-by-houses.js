function peopleByHouses(data) {
  try {
    if (typeof data != "object") {
      throw new Error("Data file is incorrect");
    } else {
      const housesData = data.houses;
      const housesWithPeopleCount = housesData.reduce(
        (accumulator, houseData) => {
          const familyName = houseData.name;
          const peopleCount = houseData.people.length;

          accumulator[familyName] = peopleCount;

          return accumulator;
        },
        {}
      );

      const sortedHousesWithPeopleCount = Object.entries(
        housesWithPeopleCount
      ).sort((houseOne, houseTwo) => {
        return houseOne[0] > houseTwo[0] ? 1 : -1;
      });

      const result = sortedHousesWithPeopleCount.reduce(
        (accumulator, currentHouseData) => {
          const houseName = currentHouseData[0];
          const peopleCount = currentHouseData[1];

          accumulator[houseName] = peopleCount;

          return accumulator;
        },
        {}
      );

      return result;
    }
  } catch (error) {
    console.log(error);
  }
}

module.exports = peopleByHouses;
