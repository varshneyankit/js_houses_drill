function peopleNameOfAllHouses(data) {
  try {
    if (typeof data != "object") {
      throw new Error("Data file is incorrect");
    } else {
      const housesData = data.houses;
      const familyNamesWithMembers = housesData.reduce(
        (accumulator, houseData) => {
          const familyName = houseData.name;
          const peopleData = houseData.people;
          const peopleNames = peopleData.map((person) => person.name);

          accumulator[familyName] = peopleNames;

          return accumulator;
        },
        {}
      );

      const sortedFamilyNamesWithMembers = Object.entries(
        familyNamesWithMembers
      ).sort((houseOne, houseTwo) => {
        return houseOne[0] > houseTwo[0] ? 1 : -1;
      });

      const result = sortedFamilyNamesWithMembers.reduce(
        (accumulator, sortData) => {
          const familyName = sortData[0];
          const peopleData = sortData[1];

          accumulator[familyName] = peopleData;

          return accumulator;
        },
        {}
      );

      return result;
    }
  } catch (error) {
    console.log(error);
  }
}

module.exports = peopleNameOfAllHouses;
