function surnameWithA(data) {
  try {
    if (typeof data != "object") {
      throw new Error("Data file is incorrect");
    } else {
      const housesData = data.houses;

      const peopleWithASurname = housesData.reduce((accumulator, houseData) => {
        const peopleData = houseData.people;

        const filteredPeopleData = peopleData.filter((person) => {
          const [firstName, lastName] = person.name.split(" ");

          if (lastName.startsWith("A")) {
            return true;
          } else {
            return false;
          }
        });

        const familyMembersWithASurname = filteredPeopleData.map(
          (person) => person.name
        );

        accumulator = accumulator.concat(familyMembersWithASurname);

        return accumulator;
      }, []);

      return peopleWithASurname;
    }
  } catch (error) {
    console.log(error);
  }
}

module.exports = surnameWithA;
