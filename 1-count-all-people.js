function countAllPeople(data) {
  try {
    if (typeof data != "object") {
      throw new Error("Data file is incorrect");
    } else {
      const housesData = data.houses;
      const totalPeopleCount = housesData.reduce((accumulator, houseData) => {
        const currentHousePeopleCount = houseData.people.length;
        return accumulator + currentHousePeopleCount;
      }, 0);

      return totalPeopleCount;
    }
  } catch (error) {
    console.log(error);
  }
}

module.exports = countAllPeople;
