function nameWithA(data) {
  try {
    if (typeof data != "object") {
      throw new Error("Data file is incorrect");
    } else {
      const housesData = data.houses;
      const peopleWithANaming = housesData.reduce((accumulator, houseData) => {
        const peopleData = houseData.people;
        const filteredPeopleData = peopleData.filter((person) => {
          const personName = person.name;

          if (personName.includes("a") || personName.includes("A")) {
            return true;
          } else {
            return false;
          }
        });

        const familyMembersWithANaming = filteredPeopleData.map(
          (person) => person.name
        );
        accumulator = accumulator.concat(familyMembersWithANaming);

        return accumulator;
      }, []);

      return peopleWithANaming;
    }
  } catch (error) {
    console.log(error);
  }
}

module.exports = nameWithA;
