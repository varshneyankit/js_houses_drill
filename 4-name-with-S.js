function nameWithS(data) {
  try {
    if (typeof data != "object") {
      throw new Error("Data file is incorrect");
    } else {
      const housesData = data.houses;
      const peopleWithSNaming = housesData.reduce((accumulator, houseData) => {
        const peopleData = houseData.people;
        const filteredPeopleData = peopleData.filter((person) => {
          const personName = person.name;

          if (personName.includes("s") || personName.includes("S")) {
            return true;
          } else {
            return false;
          }
        });

        const familyMembersWithSNaming = filteredPeopleData.map(
          (person) => person.name
        );
        accumulator = accumulator.concat(familyMembersWithSNaming);

        return accumulator;
      }, []);

      return peopleWithSNaming;
    }
  } catch (error) {
    console.log(error);
  }
}

module.exports = nameWithS;
