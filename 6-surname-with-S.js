function surnameWithS(data) {
  try {
    if (typeof data != "object") {
      throw new Error("Data file is incorrect");
    } else {
      const housesData = data.houses;

      const peopleWithSSurname = housesData.reduce((accumulator, houseData) => {
        const peopleData = houseData.people;

        const filteredPeopleData = peopleData.filter((person) => {
          const [firstName, lastName] = person.name.split(" ");

          if (lastName.startsWith("S")) {
            return true;
          } else {
            return false;
          }
        });

        const familyMembersWithSSurname = filteredPeopleData.map(
          (person) => person.name
        );

        accumulator = accumulator.concat(familyMembersWithSSurname);

        return accumulator;
      }, []);

      return peopleWithSSurname;
    }
  } catch (error) {
    console.log(error);
  }
}

module.exports = surnameWithS;
